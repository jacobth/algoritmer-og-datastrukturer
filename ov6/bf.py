#prep & read file 
with open("./fil.txt") as file:
    node_file_unsplit = file.read()

node_file = node_file_unsplit.split()

num_node = int(node_file[0])
num_kant = int(node_file[1])
node_file.pop(0) and node_file.pop(0)
#prep done


class Nodes(object):
    def __init__(self, node, pre, dist):
        self.node = node
        self.pre = pre
        self.dist = dist

class NodeTable(object):
    def __init__(self):
        self.all_cons = []

    def add_all(self, con):
                self.all_cons.append(con)

    def calculate(self, start):
        array_finished_nodes = []
        queue = []
        queue.append(Nodes(start, " ", 0))
        #while queue is not empty do:
        while queue:
            current_node = queue.pop(0)
            #loop through all even indexes, these are the one whose 'franode'
            for i in range(0, len(self.all_cons), 2):
                node_is_done = 0
                node_in_queue = 0
                if current_node.node == self.all_cons[i]:
                    #check if we've already finsihed this node 
                    for j in array_finished_nodes:
                        if self.all_cons[i+1] == j.node:
                            node_is_done = 1
                            #if we've finished this node, check if we can update distance and pre-node
                            if current_node.dist+1 < j.dist:
                                j.dist = current_node.dist+1 
                                j.pre = current_node.node
                    #check of we've already queued this node
                    for k in queue:
                        if self.all_cons[i+1] == k.node:
                            node_in_queue = 1
                            #if we've queued this node, check if we can update distance and pre-node
                            if current_node.dist+1 < j.dist:
                                j.dist = current_node.dist+1 
                                j.pre = current_node.node
                    #we've not queued or finished this node
                    if node_is_done == 0 and node_in_queue == 0:
                        #add node to queue
                        queue.append(Nodes(self.all_cons[i+1], current_node.node, current_node.dist + 1))
            #add current node to finished nodes 
            array_finished_nodes.append(current_node) 
        #sort array of x.node value 
        array_finished_nodes.sort(key=lambda x: int(x.node))  
        #print
        print("node: pre: dist:")      
        for i in array_finished_nodes:
            print(i.node, i.pre, i.dist)
        return 0

    
#init things
nt = NodeTable()
for e in node_file:
    nt.add_all(e)
start_node = input("Startnode: ")
a = nt.calculate(start_node)
