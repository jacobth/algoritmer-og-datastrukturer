
#prep and read file
with open("./fil.txt") as file:
    node_file_unsplit = file.read()

node_file = node_file_unsplit.split()

num_node = int(node_file[0])
num_kant = int(node_file[1])
node_file.pop(0) and node_file.pop(0)
#prep done


class Con(object):
    def __init__(self, node, to_node):
        self.node = node 
        self.to_node = to_node


class NodeTable(object):
    def __init__(self):
        self.all_cons = []
        self.finished_nodes = []
    
    def add_all(self, node, to_node):
                self.all_cons.append(Con(int(node), int(to_node)))

    def calculate(self, start):
        while True:
            #check if node is 'fra node', if so return this tuple, if not, return -1
            a = next((v for v in self.all_cons if v.node==start),-1)
            if a == -1:
                #check if node is 'til node', if so return tuple, if not return -1
                b = next((v for v in self.all_cons if v.to_node==start),-1)
                if b == -1:
                    #if not 'til node' and finished_nodes is not empty, means were on top of graph 
                    if self.finished_nodes:
                        #if theres still cons left, as sister on top of graph, add these to finished nodes
                        if self.all_cons:
                            for i in self.all_cons:
                                self.finished_nodes.insert(0, i.node)
                        self.finished_nodes.insert(0, start)
                        #print result and exit program
                        print(self.finished_nodes)
                        exit()
                        #if not 'fra node' or 'til node', and finished array is empty, node is not in graph
                    else:
                        raise(ValueError("Node is not in graph??"))
                    #if 'til node' insert node to finished node, remove con from all_cons and call function on parent node
                else:
                    self.finished_nodes.insert(0, start)
                    self.all_cons.remove(b)
                    nt.calculate(b.node)
            #if 'fra node'
            else:
                #if not finished call func calculate on child
                if a.to_node not in self.finished_nodes:
                    nt.calculate(a.to_node)
                #if finished, remove con from all_cons
                else:
                    self.all_cons.remove(a)


    
#init things
nt = NodeTable()
for i in range(0,len(node_file),2):
    nt.add_all(node_file[i], node_file[i+1])
start_node = int(input("Startnode: "))
try:
    nt.calculate(start_node)
#if recursion error, the graph probably contains a cycle
except RecursionError as re:
    print(re)
    print("Recursionerror! Does the graph contain a cycle?") 
