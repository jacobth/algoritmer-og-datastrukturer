import random
import timeit
#import numpy as np

arr1 = []
for i in range (0,100000):
    num = random.randint(0,100000)
    arr1.append(num)

'''arr2 = list(range(0, 100000))'''

def quickSort(arr, i):
    if len(arr) <= 1:  return arr    
    if len(arr) <= i: return selectionSort(arr)
    pivot = arr[len(arr) // 2]
    left = [x for x in arr if x < pivot]
    middle = [x for x in arr if x == pivot]
    right = [x for x in arr if x > pivot]
    return quickSort(left, i) + middle + quickSort(right, i)

def selectionSort(arr):
    if len(arr) <= 1: 
        return arr
    for i in range(0, len(arr)):
        min_idx = i
        for j in range(i+1, len(arr)):
            if arr[min_idx] > arr[j]:
                min_idx = j        
        arr[i], arr[min_idx] = arr[min_idx], arr[i]
    return arr


checksum1 = sum(arr1)
start = timeit.default_timer()
arrSorted = quickSort(arr1, 18)
stop = timeit.default_timer()
checksum2 = sum(arrSorted)
if checksum1 != checksum2:
    print("checksum error")
    exit()
for i in arrSorted:
    if i < i-1:
        print("sorting error")
        exit()    
arrSorted2 = quickSort(arrSorted, 18)
if arrSorted != arrSorted2:
    print("arrays not alike")
    exit()
else:
    print("arrays are alike")
print(stop - start)





'''
arrTimes = []
for i in range(10,25):
    checksum1 = sum(arr1)
    start = timeit.default_timer()
    arrSorted = quickSort(arr1, i)
    stop = timeit.default_timer()
    checksum2 = sum(arrSorted)
    if checksum1 != checksum2:
        print("checksum error")
        exit()
    time = stop-start
    arrTimes.append(time)
print(arrTimes.index(min(arrTimes)))


print(quickSort(arr2, 20))

'''