import random
import timeit

arr1 = []
for i in range (0,100000):
    num = random.randint(0,100000)
    arr1.append(num)


def quickSort(arr):
    if len(arr) <= 1:  return arr    
    pivot = arr[len(arr) // 2]
    left = [x for x in arr if x < pivot]
    middle = [x for x in arr if x == pivot]
    right = [x for x in arr if x > pivot]
    return quickSort(left) + middle + quickSort(right)


checksum1 = sum(arr1)
start = timeit.default_timer()
arrSorted = quickSort(arr1)
stop = timeit.default_timer()
checksum2 = sum(arrSorted)
if checksum1 != checksum2:
    print("checksum error")
    exit()
for i in arrSorted:
    if i < i-1:
        print("sorting error")
        exit()  
arrSorted2 = quickSort(arrSorted)
if arrSorted != arrSorted2:
    print("arrays not alike")
    exit()
else:
    print("arrays are alike")
print(stop - start)