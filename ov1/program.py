import timeit
import random

array= []
for j in range(0, 1000):
    array.append(random.randint(-300,300))
#print(array)


max_diff = 0
dag_kjop = 0
dag_selg = 0

start = timeit.default_timer()

for k in range( 0, len(array) ):
    temp_max_value = 0
    for i in range( k, len(array) ): 
        temp_max_value += array[i]
        if(temp_max_value > max_diff): 
            max_diff = temp_max_value
            dag_kjop = k+1 
            dag_selg = i+1

stop = timeit.default_timer()

print('max-diff', max_diff, 'kjøp på dag', dag_kjop, 'selg på dag', dag_selg)
print('Time: ', stop - start)

