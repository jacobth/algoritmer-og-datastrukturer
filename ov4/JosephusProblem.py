import sys

# Strukturen for en node i circular linked list
class Node:
    def __init__(self, x):
        self.data = x
        self.next = None


# Funksjonen lager en ciruclar linked list på størrelsen n
# Videre looper funksjonen gjennom listen og hopper over hver m-te posisjon.
# helt til det bare er en posisjon igjen. Dette er den trygge posisjonen i Josephus problem
def getLastManStanding(m, n):

    #Lager circulr linked list med størrelse=n
    head = Node(1)
    prev = head
    for i in range(2, n + 1):
        prev.next = Node(i)
        prev = prev.next
    prev.next = head  # Kobler siste node til head for å danne en circular linked list

    # Looper gjennom listen og hopper over hver m-te til det bare er en posisjon igjen.
    pos1 = head
    pos2 = head
    while (pos1.next != pos1):
        # Finner den m-te posisjonen i sirkelen
        count = 1
        while (count != m):
            pos2 = pos1
            pos1 = pos1.next
            count += 1

        # Fjerner/hopper over/Dreper hver m-te posisjon
        pos2.next = pos1.next
        pos1 = pos2.next

    print("Personen som overlever må stå på posisjon: ", pos1.data)

getLastManStanding(int(sys.argv[2]), int(sys.argv[1]))