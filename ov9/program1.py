import sys
from queue import PriorityQueue
import timeit 

class int_points (object):
    def __init__(self, id, type, name):
        self.id = id
        self.type = type 
        self.name = name

class nodes (object):
    def __init__(self, node, bredde, lengde):
        self.node = node
        self.bredde = bredde
        self.lengde = lengde 

class edges (object):
    def __init__(self, fra, til, time, lenght, speed):
        self.fra = fra
        self.til = til
        self.time = time 
        self.lenght = lenght
        self.speed = speed 

class results(object):
    def __init__(self, node, total_time, visited, in_que, prev):
        self.node = node
        self.total_time = total_time
        self.visited = visited
        self.in_que = in_que
        self.prev = prev


#prep nodes
node_arr = []
start = timeit.default_timer()
with open('./noder.txt','r') as file:
    for i in file:
        i = i.split()
        if len(i) == 1:
            num_of_nodes = int(i[0])
        else:
            node_arr.append(nodes(int(i[0]), float(i[1]), float(i[2])))

slutt = timeit.default_timer()
print('nodes loaded correct in: ', slutt - start)


edge_arr = []
#prep edges
start = timeit.default_timer()
with open('./kanter.txt','r') as file:
    for i in file:
        i = i.split()
        if len(i) == 1:
            num_of_edges= int(i[0])
        else: 
            edge_arr.append(edges(int(i[0]), int(i[1]), int(i[2]), int(i[3]), int(i[4])))
           
slutt = timeit.default_timer()
print('edges loaded correct in: ', slutt - start)


#prep intresting 
intresting_arr = []
start = timeit.default_timer()
with open('./interessepkt.txt','r') as file:
    for i in file:
        i = i.split()
        if len(i) == 1:
            num_of_intresting= int(i[0])
        else: 
            try:
                intresting_arr.append(int_points(int(i[0]), int(i[1]), ' '.join(i[2:])))
            except:
                print(i)
                exit()
slutt = timeit.default_timer()
print('int_points loaded correct in: ', slutt - start)

max_num = max(edge_arr,key=lambda item:item.time)
res_arr = []
def init_table():
    for i in range(num_of_nodes):
        res_arr.append(results(i, max_num.time+1, False, False, None))

print('prep done, files loaded to lists')

def rec_func2(node, global_fra):
    path=[]
    while node.node != global_fra:
        path.append(node.node)
        node = [a for a in res_arr if a.node == node.prev][0]
    path.append(node.node)
    return path

def find_bensin(arr):
    with open('./bensin.csv', 'w') as file:
        print('bensin to be found on disse benserne: ')
        for u in arr:
            a = next(a for a in node_arr if str(a.node) == str(u.id))
            print(u.name)
            file.write(f'{a.bredde},{a.lengde}\n')
    return

end_que = PriorityQueue()
queue = PriorityQueue()
we_soon_to_be_done = False
def dijkstras(slutt, start, bensin):
    global we_soon_to_be_done
    global queue
    node_count= 0
    bensin_arr = []
    while queue or not end_que.empty: 
        node_count +=1 
        if node_count % (num_of_nodes//100) == 0:
            print(f'searched {node_count/(num_of_nodes//100)}% of nodes')
        if we_soon_to_be_done:
            tmp_que_item = end_que.get()
            current_node = [b for b in res_arr if b.node == tmp_que_item][0]
        else:
            tmp_que_item = queue.get()
            current_node = [b for b in res_arr if b.total_time == tmp_que_item and b.in_que][0]

        for tmp_edge in edge_arr:
            if tmp_edge.fra == current_node.node:
                current_neighbour = [a for a in res_arr if a.node == tmp_edge.til][0]
                if not current_neighbour.visited and not current_neighbour.in_que:
                    current_neighbour.total_time = current_node.total_time + tmp_edge.time
                    current_neighbour.prev = current_node.node
                    if bensin:
                        a = next((a for a in intresting_arr if a.id == current_neighbour.node), -1)
                        if a != -1:
                            if a.type == 2:
                                print(f'en benser er funnet: {a.name}')
                                bensin_arr.append(a)
                                if len(bensin_arr) >= 10:
                                    find_bensin(bensin_arr)
                                    break 
                    if tmp_edge.til == slutt and not bensin:
                        current_node.visited = True
                        current_node.in_que = False
                        we_soon_to_be_done = True
                        end_que.put(tmp_edge.til)
                    else:
                        if tmp_edge.fra == slutt and not bensin:
                            end_que.put(tmp_edge.til)
                        else:
                            current_neighbour.in_que = True 
                            queue.put(current_neighbour.total_time)
                           
                elif current_neighbour.node != start:
                    if current_node.total_time + tmp_edge.time < current_neighbour.total_time:
                        old_time = current_neighbour.total_time
                        current_neighbour.total_time = tmp_edge.time + current_node.total_time
                        current_neighbour.prev = current_node.node
                        if old_time in queue.queue:
                            index_in_que = queue.queue.index(old_time)
                            queue.queue[index_in_que] = current_neighbour.total_time
            
        if we_soon_to_be_done and end_que.empty(): 
            print('Done mapping, calcualting result')
            end_node = [b for b in res_arr if b.node == slutt][0]
            travel_arr = rec_func2(end_node, start)
            travel_arr = list(reversed(travel_arr))
            print(f'total time: {end_node.total_time}\nshortest path: {"->".join(str(v) for v in travel_arr)}')
            count = 0
            for i in res_arr:
                if i.total_time != max_num.time +1:
                    count +=1 
            print('total nodes added to map: ', count)
            with open('./path.csv', 'w') as file:
                for noob in travel_arr:
                    a = next(a for a in node_arr if str(a.node) == str(noob))
                    file.write(f'{a.bredde},{a.lengde}\n')
            break 
        else:
            if we_soon_to_be_done:
                current_node.visited = True
                current_node.in_que = False
            else:
                current_node.visited = True
                current_node.in_que = False
             



def init_things():
    if len(sys.argv) > 1:
        if sys.argv[1] == '-d':
            #node_id
            start = 20
            slutt = 400
            init_table()
            queue.put(0)
            start_node = next(a for a in res_arr if a.node == start)
            start_node.total_time = 0
            start_node.in_que = True
            if len(sys.argv) > 2:
                if sys.argv[2] == '-b':
                    print('les go bensin')
                    start = timeit.default_timer()
                    dijkstras(slutt, start, True)
                    slutt = timeit.default_timer()
                    print(f'total time for locating bensin is: {slutt-start}')
            else: 
                start = timeit.default_timer()
                dijkstras(slutt, start, False)
                slutt = timeit.default_timer()
                print(f'total time for locating path is: {slutt-start}')
        elif sys.argv[1] == '-a':
            print('A* not yet implemented')
        else:
            print('Unknown argument')
    else:
        print('Missing argument')

init_things()




