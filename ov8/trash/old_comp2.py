
with open('./fil') as file:
    tekst = file.read()



def run_comp():
    text = tekst
    output = ''
    index = 0
    #lenght of the least possible string to compress 
    increment = 6
    tmp_index_to_store_counter = 0
    while True: 
        #finished
        if index + increment > len(text):
            #add the remaing string
            output += text[-increment+1:]
            #add the last counter for uncompresed chars 
            output = output[:tmp_index_to_store_counter] + f'[{(index + increment-1)-tmp_index_to_store_counter}]' + output[tmp_index_to_store_counter:]
            return output
        #if string is already processed
        if text[index: index + increment] in output:
            #check if a bigger substring also is already processed if so, compress this instead 
            while True:
                increment += 1
                if text[index: index + increment] not in output:
                    increment -=1
                    break 
            #add counter, at the index of tmp_index_to_store_counter for the previus uncompresed chars
            print(f'index: {index}, tmp_index_to_store_counter: {tmp_index_to_store_counter}, increment: {increment}')
            string_to_insert = f'[{index-tmp_index_to_store_counter}]'
            output = output[:tmp_index_to_store_counter] + string_to_insert + output[tmp_index_to_store_counter:]
            #calculate how many chars ago last occurance of the string 
            last_occurance = -(index - text.index(text[index: index + increment]))
            #add this to output in format 
            output += f'[{last_occurance},{increment}]'
            #skip to the next letter after the compress
            index += increment 
            #set new tmp_index to be the lenght of the output 
            tmp_index_to_store_counter = len(output) 
            #reset incremnet
            increment = 6
        else: 
            #if substring not procesed, add letter to output and incremnet index
            output += text[index]
            index +=1


#run func and print result
a = run_comp()
print(a)




