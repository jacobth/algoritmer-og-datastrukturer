import string 

with open('./diverse.lyx') as file:
    tekst = file.read()


extended_string = '¡¢£¤¥¦§¨©ª«¬­®¯°±²³´µ¶·¸¹º»¼½¾¿ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõö÷øùúûüýþÿ'


def run_comp():
    text = tekst
    output = ''
    index = 0
    #lenght of the least possible string to compress 
    increment = 6
    tmp_index_to_store_counter = 0
    tmp_index_in_input = 0
    extra_bytes_counter = 0
    k = 0
    while True: 
        #finished
        if index + increment >= len(text):
            diff_inc = index + increment - len(text) 
            diff = increment - diff_inc
            #add the remaing string
            output += text[-diff:len(text)]
            for j in text[-diff:len(text)]:
                if j not in string.printable and j not in extended_string:
                    extra_bytes_counter +=2

                elif j not in string.printable:
                    extra_bytes_counter +=1

            #add the last counter for uncompresed chars 
            output = output[:tmp_index_to_store_counter] + f'[{(index + increment)-tmp_index_in_input+extra_bytes_counter}]' + output[tmp_index_to_store_counter:]
            return output
        #if string is already processed
        if index > 32767:
            k = index - 32767 
        if text[index: index + increment] in text[k:index]:
            #check if a bigger substring also is already processed if so, compress this instead 
            while True:
                increment += 1
                if text[index: index + increment] not in text[k:index] or index + increment >= len(text):
                    increment -=1
                    break     
            #add counter, at the index of tmp_index_to_store_counter for the previus uncompresed chars 
            string_to_insert = f'[{index-tmp_index_in_input+extra_bytes_counter}]'
            output = output[:tmp_index_to_store_counter] + string_to_insert + output[tmp_index_to_store_counter:]
            #calculate how many chars ago last occurance of the string 
            last_occurance = -(index - text[:index].rindex(text[index: index + increment]))
            #add this to output in format 
            output += f'[{last_occurance},{increment}]'
            #skip to the next letter after the compress
            index += increment 
            #set new tmp_index to be the lenght of the output 
            tmp_index_to_store_counter = len(output) 
            tmp_index_in_input = index 
            #reset incremnet
            increment = 6
            extra_bytes_counter = 0
        else: 
            #if substring not procesed, add letter to output and incremnet index
            output += text[index]
            if text[index] not in string.printable and text[index] not in extended_string:
                extra_bytes_counter +=2
            elif text[index] not in string.printable:
                extra_bytes_counter +=1
            index +=1



#run func and print result
a = run_comp()
with open('./LZ_compressed_text', 'w') as file:
    file.write('')

#print(a)
i = 0
extra_bytes_counter2 = 0
togle_replace = False
togle_append_clear_text = False
while True:
    if i >= len(a):
        break
    if i == 1 or togle_append_clear_text == True: 
        len_of_int = 0
        while True:
            if a[i+len_of_int] == ']':
                break
            len_of_int +=1
        with open('./LZ_compressed_text', 'ab') as file:
            int1 = int(a[i:i+len_of_int])
            file.write(int1.to_bytes(2,'little'))
        with open('./LZ_compressed_text', 'a') as file:
            #print(a[i + len_of_int+1:i + len_of_int+1+int1])
            for j in a[i + len_of_int+1:i + len_of_int+1+int1]:
                if j not in string.printable and j not in extended_string:
                    extra_bytes_counter2 += 2
                elif j not in string.printable:
                    extra_bytes_counter2 += 1
            file.write(a[i + len_of_int+1:i + len_of_int+1+int1 - extra_bytes_counter2])
        i += len_of_int + int1 + 2 - extra_bytes_counter2
        togle_replace = True
        togle_append_clear_text = False
        extra_bytes_counter2 = 0
    
    elif togle_replace == True: 
        len_of_backtrack_int = i
        while True:
            if a[len_of_backtrack_int] == ',':
                break 
            len_of_backtrack_int +=1
        backtrack_value = int(a[i:len_of_backtrack_int])
        diff = len_of_backtrack_int - i 
        backtrack_value = backtrack_value & 0xffff
        i += diff +1
        len_of_backtrack = i
        while True:
            if a[len_of_backtrack] == ']':
                break
            len_of_backtrack +=1
        len_of_backtrack_value = int(a[i:len_of_backtrack])
        with open('./LZ_compressed_text', 'ab') as file:
            file.write(backtrack_value.to_bytes(2,'little'))
            file.write(len_of_backtrack_value.to_bytes(2,'little'))
        togle_append_clear_text = True
        togle_replace = False
        diff = len_of_backtrack - i
        i += diff +2 
    else:
        i+=1


