


with open('./LZ_compressed_text','rb') as file:
    tekst = file.read()

class Table_object(object):
    def __init__(self, char, freq):
        self.char = char
        self.freq = freq

all_lz_bytes = []
strings_array= []
def dcomp():
    i = 0
    dcomp_string = ''
    togle_not_bytes = False
    while i < (len(tekst)):
        if togle_not_bytes:
            dcomp_string += tekst[i:i+num].decode('utf-8')
            strings_array.append(tekst[i:i+num].decode('utf-8'))
            togle_not_bytes = False
            if i + num + 4 < len(tekst):
                all_lz_bytes.append(tekst[i + num: i + num + 2])
                all_lz_bytes.append(tekst[i + num + 2: i + num + 4])
            i += num + 4
        else:
            value = tekst[i:i+2]
            value = int.from_bytes(value, "little")
            togle_not_bytes = True
            num = value
            all_lz_bytes.append(tekst[i:i+2])
            i+=2
        
    return dcomp_string

all_strings = dcomp()


table = []

for i in all_strings:
    a = [a for a in table if a.char == i]
    if a:
        a[0].freq +=1 
    else:
        table.append(Table_object(i, 1))

table = sorted(table,key=lambda x: x.freq)


class node:
	def __init__(self, freq, symbol, left=None, right=None):
		self.freq = freq
		self.symbol = symbol
		self.left = left
		self.right = right
		self.huff = ''


rep_tree_arr = []
class rep_tree():
    def __init__(self, symb, val):
        self.symb = symb
        self.val = val


def printNodes(node, val=''):
    newVal = val + str(node.huff)

    if(node.left):
        printNodes(node.left, newVal)   
    if(node.right):
        printNodes(node.right, newVal)

    if(not node.left and not node.right):
        rep_tree_arr.append(rep_tree(node.symbol, newVal))


nodes = []
for j in table:
	nodes.append(node(j.freq, j.char))

while len(nodes) > 1:

	left = nodes[0]
	right = nodes[1]
	left.huff = 0
	right.huff = 1
	newNode = node(left.freq+right.freq, left.symbol+right.symbol, left, right)
	nodes.remove(left)
	nodes.remove(right)
	nodes.append(newNode)

printNodes(nodes[0])


bit_string = ''
		

for i in all_strings:
    a = [a for a in rep_tree_arr if a.symb == i]
    bit_string += a[0].val



if len(bit_string)%8 != 0:
    rest = 8 - len(bit_string)%8
    bit_string += '0' * rest


bytes = []

for i in range(0 ,len(bit_string), 8):
    num = int(bit_string[i:i+8], 2)
    bytes.append(num.to_bytes(1,'little'))

num_of_bytes = len(bytes)

with open('./HM_compressed_text', 'w') as file: 
    for i in rep_tree_arr:
        file.write(f'{i.symb}{i.val}\n')
    file.write(f'{num_of_bytes} {rest} END TABLE \n')


with open('./HM_compressed_text', 'ab') as file: 
    for i in bytes:
        file.write(i)



with open('./HM_compressed_text', 'ab') as file: 
    for i in all_lz_bytes:
        file.write(i)

