
Lempel Ziv og Huffman komprimering

Komprimeringen blir gjort på filen './fil.txt' for å endre dette kan du endre på 3. linje i LZ_compression.py

Komprimeringen foregår i 2 steg:
1. LZ_compression.py komprimerer innholdet i fil.txt med lempel ziv til filen LZ_compressed_text
2. HM_compression.py komprimerer innhodet i LZ_compressed_text til filen HM_compressed_text

Når komprimeringen er ferdig har man fått en fil med sansynligvis vesentlig mindre størelse

Dekomprimeringen foregår i motsatte 2 steg:
1. HM_uncompression.py dekomprimerer inholdet fra HM_compressed_text til HM_uncompressed_text
2. LZ_uncompression.py dekomprimerer innholdet fra HM_uncompressed_text til LZ_uncompressed_text 

Når dekomprimeringen er ferdig sitter man igjen med LZ_uncompressed_text som skal være identisk med fil.txt
